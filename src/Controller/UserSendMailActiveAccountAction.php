<?php


namespace App\Controller;

use ApiPlatform\Core\DataProvider\Pagination;
use App\Entity\CategorieRessource;
use App\Entity\User;
use App\Repository\CategorieRessourceRepository;
use App\Repository\UserRepository;
use App\Service\SendMail;
use App\Service\UserActiveAccount;
use Doctrine\ORM\Query\QueryException;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class UserSendMailActiveAccountAction
{
    private $activeCompte;

    public function __construct(UserActiveAccount $activeCompte)
    {
        $this->activeCompte = $activeCompte;
    }

    /**
     * @param User $data
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws LoaderError
     */
    public function __invoke(User $data)
    {
        $this->activeCompte->sendMailActivation($data);
    }

}