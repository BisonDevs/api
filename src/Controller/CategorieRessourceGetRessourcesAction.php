<?php


namespace App\Controller;

use App\Entity\CategorieRessource;
use App\Repository\CategorieRessourceRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Exception;
use Symfony\Component\HttpFoundation\Request;

class CategorieRessourceGetRessourcesAction extends PaginateListAction
{
    /**
     * @param CategorieRessource $data
     * @param Request $request
     * @param CategorieRessourceRepository $categorieRessourceRepository
     * @return Paginator
     * @throws Exception
     */
    public function __invoke(CategorieRessource $data, Request $request, CategorieRessourceRepository $categorieRessourceRepository): Paginator
    {
        return $this->paginator->getPaginateListFromRepo($categorieRessourceRepository, $request, $data->getId(), 'getListeRessources');
    }
}