<?php

namespace App\Controller;

use App\Entity\PasswordGenerate;
use App\Service\PasswordGenerator;
use stdClass;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class PasswordGenerateController extends AbstractController
{
    /**
     * @param PasswordGenerator $passwordGenerator
     * @return JsonResponse
     * @Route(path="/api/generate_password", name="generate_password")
     */
    public function generate(PasswordGenerator $passwordGenerator): JsonResponse
    {
        $generatePassword = new PasswordGenerate($passwordGenerator);
        $obj = new stdClass();
        $obj->generatedPassword = $generatePassword->getGeneratedPassword();
        return new JsonResponse($obj, 200);
    }
}