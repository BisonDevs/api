<?php

namespace App\Controller;

use App\Entity\PasswordGenerate;
use App\Entity\User;
use App\Service\PasswordGenerator;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\User\UserInterface;

class PasswordResetAction
{
    /**
     * @var PasswordGenerator
     */
    private $passwordGenerator;

    public function __construct(PasswordGenerator $passwordGenerator)
    {
        $this->passwordGenerator = $passwordGenerator;
    }

    public function __invoke(User $data)
    {
        $newPassword = $this->passwordGenerator->generate();
        $user->setPlainPassword($newPassword, true);

        // TODO Send email with new password

        return $user;
    }
}