<?php


namespace App\Controller;

use ApiPlatform\Core\DataProvider\Pagination;
use App\Entity\CategorieRessource;
use App\Entity\User;
use App\Repository\CategorieRessourceRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\Query\QueryException;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Exception;
use Symfony\Component\HttpFoundation\Request;

class UserGetRessourcesAction extends PaginateListAction
{

    /**
     * @param User $data
     * @param Request $request
     * @param UserRepository $userRepository
     * @return Paginator
     * @throws Exception
     */
    public function __invoke(User $data, Request $request, UserRepository $userRepository): Paginator
    {
        return $this->paginator->getPaginateListFromRepo($userRepository, $request, $data->getId(), 'getListeRessources');
    }
}