<?php


namespace App\Controller;


use App\Service\PaginateList;

abstract class PaginateListAction
{

    protected PaginateList $paginator;

    public function __construct(PaginateList $paginatedListe)
    {
        $this->paginator = $paginatedListe;
    }

}