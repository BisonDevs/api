<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Exception;
use Symfony\Component\HttpFoundation\Request;

class UserGetAvailableUserRelation extends PaginateListAction
{
    /**
     * @param User $data
     * @param Request $request
     * @param UserRepository $userRepository
     * @return Paginator
     * @throws Exception
     */
    public function __invoke(User $data, Request $request, UserRepository $userRepository)
    {
        return $this->paginator->getPaginateListFromRepo($userRepository, $request, $data->getId(), 'getListeUtilisateurDispoRelation');
    }
}