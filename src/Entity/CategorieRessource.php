<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CategorieRessourceRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use App\Controller\CategorieRessourceGetRessourcesAction;

/**
 * @ORM\Entity(repositoryClass=CategorieRessourceRepository::class)
 * @ApiResource(
 *     itemOperations={
 *          "get", "put", "patch",
 *          "liste_ressources"={
 *          "method"="GET",
 *          "path"="/categorie_ressources/{id}/liste_ressources",
 *          "controller"=CategorieRessourceGetRessourcesAction::class,
 *       }
 *     },
 *      normalizationContext={
 *     "groups"={"categorie_ressource.read", "categorie_ressource.all"}
 *     },
 *     denormalizationContext={
 *      "groups"={"categorie_ressource.write", "categorie_ressource.all"}
 *     },
 *     attributes={
 *     "pagination_enabled"=false
 *     }
 * )
 */
class CategorieRessource
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ApiProperty(identifier=false)
     */
    protected $id;

    /**
     * @var String
     * @ORM\Column(type="string", unique=true)
     * @ApiProperty(identifier=true)
     * @Gedmo\Slug(fields={"libelle"})
     * @Groups({
     *     "categorie_ressource.read",
     *     "ressource.read",
     *     "user.read",
     * })
     */
    protected $slug;

    /**
     * @Gedmo\Locale
     */
    protected $locale;

///*
//    /**
//     * @ORM\OneToMany(targetEntity=Ressource::class, mappedBy="categorieRessource", orphanRemoval=true)
//     * @ApiProperty(readableLink=false, writableLink=false)
//     * @Groups({
//     *     "categorie_ressource.liste_ressources"
//     * })
//     */
//    protected $lsRessources;*/

    /**
     * @var String
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Veuillez renseigner le libelle")
     * @Gedmo\Language
     * @Groups({
     *     "categorie_ressource.all",
     *     "ressource.read",
     *     "user.read",
     * })
     */
    protected $libelle;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     * @Groups({
     *     "categorie_ressource.read"
     * })
     */
    protected $createdDate;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     * @Groups({
     *     "categorie_ressource.read"
     * })
     */
    protected $updatedDate;

//
//    /**
//     * @var DateTime
//     * @ORM\Column(type="datetime", nullable=true)
//     * @Groups({
//     *     "categorie_ressource.read"
//     * })
//     */
//    protected $deletedDate;

//    public function __construct()
//    {
//        $this->lsRessources = new ArrayCollection();
//    }
//
//    /**
//     * @return Collection|Ressource[]
//     */
//    public function getLsRessources(): Collection
//    {
//        return $this->lsRessources;
//    }
//
//    public function addLsRessource(Ressource $lsRessource): self
//    {
//        if (!$this->lsRessources->contains($lsRessource)) {
//            $this->lsRessources[] = $lsRessource;
//            $lsRessource->setCategorieRessource($this);
//        }
//
//        return $this;
//    }
//
//    public function removeLsRessource(Ressource $lsRessource): self
//    {
//        if ($this->lsRessources->removeElement($lsRessource)) {
//            // set the owning side to null (unless already changed)
//            if ($lsRessource->getCategorieRessource() === $this) {
//                $lsRessource->setCategorieRessource(null);
//            }
//        }
//
//        return $this;
//    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreatedDate(): DateTime
    {
        return $this->createdDate;
    }

    public function getUpdatedDate(): ?DateTime
    {
        return $this->updatedDate;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getLibelle()
    {
        return $this->libelle;
    }
//
//    public function getDeletedDate(): ?DateTime
//    {
//        return $this->deletedDate;
//    }
//
//    public function delete(bool $delete): self
//    {
//        if ($delete) {
//            $this->deletedDate = new DateTime();
//        } else {
//            $this->deletedDate = new DateTime();
//        }
//    }

    public function getLocale()
    {
        return $this->locale;
    }

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }

}
