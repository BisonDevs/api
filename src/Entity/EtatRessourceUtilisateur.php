<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\EtatRessourceUtilisateurRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ORM\Entity(repositoryClass=EtatRessourceUtilisateurRepository::class)
 * @ApiResource(
 *     attributes={
 *     "normalization_context"={"groups"={"etat_ressource_utilisateur.read", "etat_ressource_utilisateur.all"}},
 *     "denormalization_context"={"groups"={"etat_ressource_utilisateur.write", "etat_ressource_utilisateur.all"}}
 * }
 * )
 * @ApiFilter(SearchFilter::class,
 *  properties={
 *     "utilisateur.uuid": "exact",
 *  }
 * )
 */
class EtatRessourceUtilisateur implements CreateByUser
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({
     *     "etat_ressource_utilisateur.all",
     *     "ressource.read",
     *     "user.read",
     * })
     */
    protected $id;

//    , inversedBy="etatRessourceUtilisateurs"
    /**
     * @ORM\ManyToOne(targetEntity=Ressource::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({
     *     "etat_ressource_utilisateur.all",
     *     "user.read",
     *     "etat_ressource.read"
     * })
     */
    protected $ressource;

//, inversedBy="lsEtatRessourceUtilisateur"
    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({
     *     "etat_ressource_utilisateur.all",
     *     "ressource.read",
     *     "etat_ressource.read",
     * })
     */
    protected $utilisateur;

//    , inversedBy="lsRessourcesUtilisateurs"
    /**
     * @ORM\ManyToOne(targetEntity=EtatRessource::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({
     *     "etat_ressource_utilisateur.all",
     *     "ressource.read",
     *     "user.read",
     * })
     */
    protected $etatRessource;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     * @Groups({
     *     "etat_ressource_utilisateur.read"
     * })
     */
    protected $createdDate;

    public function getRessource(): ?Ressource
    {
        return $this->ressource;
    }

    public function setRessource(?Ressource $ressource): self
    {
        $this->ressource = $ressource;

        return $this;
    }

    public function getUtilisateur(): ?User
    {
        return $this->utilisateur;
    }

    public function setUtilisateur(?User $utilisateur): self
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    public function getEtatRessource(): ?EtatRessource
    {
        return $this->etatRessource;
    }

    public function setEtatRessource(?EtatRessource $etatRessource): self
    {
        $this->etatRessource = $etatRessource;

        return $this;
    }

    public function getCreatedDate(): DateTime
    {
        return $this->createdDate;
    }

    public function getUser(): ?UserInterface
    {
        return $this->getUtilisateur();
    }

    public function setUser(UserInterface $user)
    {
        return $this->setUtilisateur($user);
    }

    /**
     * @return int id
     */
    public function getId()
    {
        return $this->id;
    }
}
