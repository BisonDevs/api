<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PartageRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=PartageRepository::class)
 * @ApiResource(
 *     itemOperations={
 *      "get"={"security_post_denormalize"="is_granted('ROLE_ADMIN') or (object.getPartageur() == user)"},
 *      "delete"={"security_post_denormalize"="is_granted('ROLE_ADMIN') or (object.getPartageur() == user)"}
 *     },
 *     collectionOperations={
 *      "post"={"security_post_denormalize"="is_granted('ROLE_ADMIN') or (object.getPartageur() == user and is_granted('ROLE_CITOYEN'))"}
 *     },
 *     normalizationContext={"groups"={"partage.read","partage.all"}},
 *     denormalizationContext={"groups"={"partage.write", "partage.all"}}
 * )
 */
class Partage implements CreateByUser
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({
     *     "partage.all",
     *     "type_relation.read",
     *     "ressource.read"
     * })
     */
    protected $id;

//    , inversedBy="lsPartages"
    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull(message="Veuillez renseigner l'utlisateur qui partage la ressource")
     * @Groups({
     *     "partage.all",
     *     "type_relation.read",
     *     "ressource.read"
     * })
     */
    protected $partageur;

    /**
     * @ORM\ManyToOne(targetEntity=TypeRelation::class)
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull(message="Veuillez renseigner le type de relation pour lequelle la ressource sera partagée")
     * @Groups({
     *     "partage.all",
     *     "user.read",
     *     "ressource.read"
     * })
     */
    protected $ciblePartage;

//, inversedBy="lsPartages"
    /**
     * @ORM\ManyToOne(targetEntity=Ressource::class)
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull(message="Veuillez renseigner la ressource à partager")
     * @Groups({
     *     "partage.all",
     *     "type_relation.read",
     *     "user.read"
     * })
     */
    protected $ressourcePartage;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     * @Groups({
     *     "partage.read",
     * })
     */
    protected $createdDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({
     *     "partage.read",
     * })
     */
    private $activatedDate;

    public function getPartageur(): ?User
    {
        return $this->partageur;
    }

    public function setPartageur(?User $partageur): self
    {
        $this->partageur = $partageur;

        return $this;
    }

    public function getCiblePartage(): ?TypeRelation
    {
        return $this->ciblePartage;
    }

    public function setCiblePartage(?TypeRelation $ciblePartage): self
    {
        $this->ciblePartage = $ciblePartage;

        return $this;
    }

    public function getRessourcePartage(): ?Ressource
    {
        return $this->ressourcePartage;
    }

    public function setRessourcePartage(?Ressource $ressourcePartage): self
    {
        $this->ressourcePartage = $ressourcePartage;

        return $this;
    }

    public function getCreatedDate(): DateTime
    {
        return $this->createdDate;
    }

    public function getActivatedDate(): ?\DateTimeInterface
    {
        return $this->activatedDate;
    }

    public function initActivatedDate(): self
    {
        if(empty($this->activatedDate)){
            $this->activatedDate = new DateTime();
        }

        return $this;
    }

    public function getUser(): ?UserInterface
    {
        return $this->getPartageur();
    }

    public function setUser(UserInterface $user)
    {
        return $this->setPartageur($user);
    }

    /**
     * @return int id
     */
    public function getId()
    {
        return $this->id;
    }
}
