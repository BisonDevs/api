<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\PasswordResetAction;
use App\Controller\UserActivateAccountAction;
use App\Controller\UserGetAvailableUserRelation;
use App\Controller\UserGetPendingRelations;
use App\Controller\UserGetPendingRequest;
use App\Controller\UserGetRelations;
use App\Controller\UserGetRessourcesAction;
use App\Controller\UserGetUserInfo;
use App\Controller\UserSendMailActiveAccountAction;
use App\Repository\UserRepository;
use App\Service\UuidService;
use App\Validator\UniqueEmail;
use DateTime;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;
use App\Filter\CustomUserFilter;


/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(fields="email", message="Cet email est déjà utilisé")
 * @UniqueEntity(fields="pseudo", message="Ce pseudo est déjà utilisé")
 * @UniqueEntity(fields="uuid", message="Une erreur interne est survenue")
 * @ApiResource(
 *     itemOperations={
 *      "get",
 *      "put"={"security_post_denormalize"="is_granted('ROLE_ADMIN') or (object == user)"},
 *     "patch"={"security_post_denormalize"="is_granted('ROLE_ADMIN') or (object == user)"},
 *     "get_relation_of_user"={
 *              "method"="GET",
 *              "path"="/users/{id}/relations",
 *              "controller"=UserGetRelations::class,
 *              "security_post_denormalize"="is_granted('ROLE_SUPER_ADMIN') or (object == user)"
 *      },
 *     "get_pending_relation_of_user"={
 *              "method"="GET",
 *              "path"="/users/{id}/receptions_en_attente",
 *              "controller"=UserGetPendingRelations::class,
 *              "security_post_denormalize"="is_granted('ROLE_SUPER_ADMIN') or (object == user)"
 *      },
 *     "get_user_info_from_other"={
 *              "method"="GET",
 *              "path"="/users/{id}/get_infos",
 *              "controller"=UserGetUserInfo::class,
 *              "normalization_context"={"groups"={"user.read", "user.all", "user.get_info"}}
 *      },
 *     "get_pending_request_of_user"={
 *              "method"="GET",
 *              "path"="/users/{id}/demandes_en_attente",
 *              "controller"=UserGetPendingRequest::class,
 *              "security_post_denormalize"="is_granted('ROLE_SUPER_ADMIN') or (object == user)"
 *      },
 *     "get_available_users_for_relation"={
 *              "method"="GET",
 *              "path"="/users/{id}/utilisateur_sans_relation",
 *              "controller"=UserGetAvailableUserRelation::class,
 *              "security_post_denormalize"="is_granted('ROLE_SUPER_ADMIN') or (object == user)"
 *      },
 *       "generate_password"={
 *          "method"="GET",
 *          "path"="/users/{id}/reset_password",
 *          "controller"=PasswordResetAction::class,
 *          "security_post_denormalize"="is_granted('ROLE_ADMIN') or (object == user)"
 *       },
 *       "liste_ressources"={
 *          "method"="GET",
 *          "path"="/users/{id}/liste_ressources",
 *          "controller"=UserGetRessourcesAction::class
 *       },
 *       "send_mail_active_account"={
 *          "method"="GET",
 *          "path"="/users/{id}/envoie_mail_active_compte",
 *          "controller"=UserSendMailActiveAccountAction::class
 *       }
 *     },
 *     collectionOperations={
 *      "get",
 *      "post",
 *       "active_account"={
 *          "method"="GET",
 *          "path"="/users/{token}/active_compte",
 *          "controller"=UserActivateAccountAction::class
 *          }
 *     },
 *    normalizationContext={
 *     "groups"={"user.read", "user.all"}
 *     },
 *     denormalizationContext={
 *      "groups"={"user.write", "user.all"}
 *     },
 * )
 * @ApiFilter(CustomUserFilter::class,
 *  properties={
 *     "custom_user_filter"
 *   }
 * )
 * @ORM\HasLifecycleCallbacks
 */
class User implements UserInterface
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ApiProperty(identifier=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=50, unique=true)
     * @ApiProperty(identifier=true)
     * @Groups({
     *     "user.read",
     *     "ressource.read",
     * })
     */
    protected $uuid;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\Email(message="Veuillez renseigner une adresse mail valide")
     * @Assert\NotBlank(message="Veuillez renseigner une adresse mail valide")
     * @Groups({
     *     "user.all",
     *     "ressource.read",
     * })
     */
    protected $email;

    /**
     * @ORM\Column(type="json")
     * @Groups({
     *     "user.read",
     *     "ressource.read",
     * })
     */
    protected $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    protected $password;

    /**
     * @ORM\Column(type="string", length=30)
     * @Assert\NotBlank(message="Veuillez renseigner un nom")
     * @Assert\Length(max="30", maxMessage="Veuillez renseigner un nom avec moins de 30 caractères")
     * @Groups({
     *     "user.all",
     *     "ressource.read",
     * })
     */
    protected $nom;

    /**
     * @ORM\Column(type="string", length=30)
     * @Assert\NotBlank(message="Veuillez renseigner un prénom")
     * @Assert\Length(max="30", maxMessage="Veuillez renseigner un prénom avec moins de 30 caractères")
     * @Groups({
     *     "user.all",
     *     "ressource.read",
     * })
     */
    protected $prenom;

    /**
     * @ORM\Column(type="string", length=30, nullable=true, unique=true)
     * @Assert\Length(max="30", maxMessage="Veuillez renseigner un pseudo avec moins de 30 caractères")
     * @Groups({
     *   "user.all"
     * })
     */
    protected $pseudo;

    /**
     * @var MediaObject|null
     * @Groups({
     *     "user.all",
     *     "ressource.read",
     * })
     * @ORM\ManyToOne(targetEntity=MediaObject::class)
     * @ORM\JoinColumn(nullable=true)
     * @ApiProperty(iri="http://schema.org/image")
     */
    protected $image;

    /**
     * @Groups({"user.write"})
     * @SerializedName("password")
     * @Assert\NotNull(message="Veuillez renseigner un mot de passe valide (2 majuscules, 2 minuscules, 2 caractères spéciaux, 2 chiffres)")
     * @Assert\Regex(
     *     pattern="/(?=.*[A-Z].*[A-Z])(?=.*[^\W].*[^\W])(?=.*[0-9].*[0-9])(?=.*[a-z].*[a-z]).{8,50}/",
     *     match=true,
     *     message="Veuillez renseigner un mot de passe valide avec 2 majuscules, 2 minuscules, 2 caractères spéciaux, 2 chiffres et maximum 50 caractères"
     * )
     */
    protected $plainPassword;

    /**
     * @Groups({"user.get_info"})
     */
    public array $relations = [];
//
//    /**
//     * @Groups({"user.read"})
//     * @ORM\OneToMany(targetEntity=Ressource::class, mappedBy="createur")
//     * @ApiProperty(readableLink=false, writableLink=false)
//     * @Groups({
//     *     "user.read"
//     * })
//     */
//    protected $lsRessources;
//
//    /**
//     * @ORM\OneToMany(targetEntity=Partage::class, mappedBy="partageur", orphanRemoval=true)
//     * @ApiProperty(readableLink=false, writableLink=false)
//     * @Groups({
//     *     "user.read"
//     * })
//     */
//    protected $lsPartages;
//
//    /**
//     * @ORM\OneToMany(targetEntity=Relation::class, mappedBy="demandeur", orphanRemoval=true)
//     * @ApiProperty(readableLink=false, writableLink=false)
//     * @Groups({
//     *     "user.read"
//     * })
//     */
//    protected $lsRelations;
//
//    /**
//     * @ORM\OneToMany(targetEntity=Relation::class, mappedBy="receveur", orphanRemoval=true)
//     * @ApiProperty(readableLink=false, writableLink=false)
//     */
//    protected $relationsRecues;
//
//    /**
//     * @ORM\ManyToMany(targetEntity=Discussion::class, mappedBy="participants")
//     * @ApiProperty(readableLink=false, writableLink=false)
//     */
//    protected $lsDiscussions;
//
//    /**
//     * @ORM\OneToMany(targetEntity=Message::class, mappedBy="auteur")
//     * @ApiProperty(readableLink=false, writableLink=false)
//     */
//    protected $lsMessages;
//
//    /**
//     * @ORM\OneToMany(targetEntity=Commentaire::class, mappedBy="ecritPar")
//     * @ApiProperty(readableLink=false, writableLink=false)
//     */
//    protected $lsCommentaires;
//
//    /**
//     * @ORM\OneToMany(targetEntity=EtatRessourceUtilisateur::class, mappedBy="utilisateur")
//     * @ApiProperty(readableLink=false, writableLink=false)
//     */
//    protected $lsEtatRessourceUtilisateur;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     * @Groups({
     *     "user.read"
     * })
     */
    protected $createdDate;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     * @Groups({
     *     "user.read"
     * })
     */
    protected $updatedDate;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({
     *     "user.read"
     * })
     */
    protected $deletedDate;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({
     *     "user.read"
     * })
     */
    protected $resetDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({
     *     "user.read"
     * })
     */
    private $activatedDate;
//
//    public function __construct()
//    {
//        $this->lsRessources = new ArrayCollection();
//        $this->lsPartages = new ArrayCollection();
//        $this->lsRelations = new ArrayCollection();
//        $this->relationsRecues = new ArrayCollection();
//        $this->lsDiscussions = new ArrayCollection();
//        $this->lsMessages = new ArrayCollection();
//        $this->lsCommentaires = new ArrayCollection();
//        $this->lsEtatRessourceUtilisateur = new ArrayCollection();
//    }

    /**
     * @return int id
     */
    public function getId()
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string)$this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        if (empty($roles)) {
            // guarantee every user at least has ROLE_USER
            $roles[] = (empty($this->activatedDate) ? 'ROLE_USER' : 'ROLE_CITOYEN');
        }

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        $this->plainPassword = null;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(string $plainPassword, $hasBeenReset = false): self
    {
        $this->plainPassword = $plainPassword;

        if ($hasBeenReset) {
            $this->resetDate = new DateTime();
        }

        return $this;
    }

    /**
     * @ORM\PrePersist()
     * @param LifecycleEventArgs $eventArgs
     * @throws Exception
     */
    public function initUser(LifecycleEventArgs $eventArgs)
    {
        $uuid_generateur = new UuidService($eventArgs->getEntityManager());
        $this->uuid = $uuid_generateur->generateUuid(get_class($this));
    }
//
//    /**
//     * @return Collection|Ressource[]
//     */
//    public function getLsRessources(): Collection
//    {
//        return $this->lsRessources;
//    }
//
//    public function addLsRessource(Ressource $lsRessource): self
//    {
//        if (!$this->lsRessources->contains($lsRessource)) {
//            $this->lsRessources[] = $lsRessource;
//            $lsRessource->setCreateur($this);
//        }
//
//        return $this;
//    }
//
//    public function removeLsRessource(Ressource $lsRessource): self
//    {
//        if ($this->lsRessources->removeElement($lsRessource)) {
//            // set the owning side to null (unless already changed)
//            if ($lsRessource->getCreateur() === $this) {
//                $lsRessource->setCreateur(null);
//            }
//        }
//
//        return $this;
//    }

    public function getCreatedDate(): DateTime
    {
        return $this->createdDate;
    }

    public function getDeletedDate(): ?DateTime
    {
        return $this->deletedDate;
    }


    public function delete(bool $deleted): self
    {
        if ($deleted) {
            $this->deletedDate = new DateTime();
        } else {
            $this->deletedDate = new DateTime();
        }
    }

    public function getUpdatedDate(): ?DateTime
    {
        return $this->updatedDate;
    }

    /**
     * @Groups({
     *     "user.read",
     *     "ressource.read",
     *     })
     * @SerializedName("displayedName")
     * @return string
     */
    public function getDisplayName(): string
    {
        if (empty($this->pseudo)) {
            return strtoupper($this->nom) . " " . $this->prenom;
        } else {
            return $this->pseudo;
        }
    }
//
//    /**
//     * @return Collection|Partage[]
//     */
//    public function getLsPartages(): Collection
//    {
//        return $this->lsPartages;
//    }
//
//    public function addLsPartage(Partage $lsPartage): self
//    {
//        if (!$this->lsPartages->contains($lsPartage)) {
//            $this->lsPartages[] = $lsPartage;
//            $lsPartage->setPartageur($this);
//        }
//
//        return $this;
//    }
//
//    public function removeLsPartage(Partage $lsPartage): self
//    {
//        if ($this->lsPartages->removeElement($lsPartage)) {
//            // set the owning side to null (unless already changed)
//            if ($lsPartage->getPartageur() === $this) {
//                $lsPartage->setPartageur(null);
//            }
//        }
//
//        return $this;
//    }
//
//    /**
//     * @return Collection|Relation[]
//     */
//    public function getLsRelations(): Collection
//    {
//        return $this->lsRelations;
//    }
//
//    public function addLsRelation(Relation $lsRelation): self
//    {
//        if (!$this->lsRelations->contains($lsRelation)) {
//            $this->lsRelations[] = $lsRelation;
//            $lsRelation->setDemandeur($this);
//        }
//
//        return $this;
//    }
//
//    public function removeLsRelation(Relation $lsRelation): self
//    {
//        if ($this->lsRelations->removeElement($lsRelation)) {
//            // set the owning side to null (unless already changed)
//            if ($lsRelation->getDemandeur() === $this) {
//                $lsRelation->setDemandeur(null);
//            }
//        }
//
//        return $this;
//    }
//
//    /**
//     * @return Collection|Relation[]
//     */
//    public function getRelationsRecues(): Collection
//    {
//        return $this->relationsRecues;
//    }
//
//    public function addRelationsRecue(Relation $relationsRecue): self
//    {
//        if (!$this->relationsRecues->contains($relationsRecue)) {
//            $this->relationsRecues[] = $relationsRecue;
//            $relationsRecue->setReceveur($this);
//        }
//
//        return $this;
//    }
//
//    public function removeRelationsRecue(Relation $relationsRecue): self
//    {
//        if ($this->relationsRecues->removeElement($relationsRecue)) {
//            // set the owning side to null (unless already changed)
//            if ($relationsRecue->getReceveur() === $this) {
//                $relationsRecue->setReceveur(null);
//            }
//        }
//
//        return $this;
//    }
//
//    /**
//     * @return Collection|Discussion[]
//     */
//    public function getLsDiscussions(): Collection
//    {
//        return $this->lsDiscussions;
//    }
//
//    public function addLsDiscussion(Discussion $lsDiscussion): self
//    {
//        if (!$this->lsDiscussions->contains($lsDiscussion)) {
//            $this->lsDiscussions[] = $lsDiscussion;
//            $lsDiscussion->addParticipant($this);
//        }
//
//        return $this;
//    }
//
//    public function removeLsDiscussion(Discussion $lsDiscussion): self
//    {
//        if ($this->lsDiscussions->removeElement($lsDiscussion)) {
//            $lsDiscussion->removeParticipant($this);
//        }
//
//        return $this;
//    }
//
//    /**
//     * @return Collection|Message[]
//     */
//    public function getLsMessages(): Collection
//    {
//        return $this->lsMessages;
//    }
//
//    public function addLsMessage(Message $lsMessage): self
//    {
//        if (!$this->lsMessages->contains($lsMessage)) {
//            $this->lsMessages[] = $lsMessage;
//            $lsMessage->setAuteur($this);
//        }
//
//        return $this;
//    }
//
//    public function removeLsMessage(Message $lsMessage): self
//    {
//        if ($this->lsMessages->removeElement($lsMessage)) {
//            // set the owning side to null (unless already changed)
//            if ($lsMessage->getAuteur() === $this) {
//                $lsMessage->setAuteur(null);
//            }
//        }
//
//        return $this;
//    }
//
//    /**
//     * @return Collection|Message[]
//     */
//    public function getLsCommentaires(): Collection
//    {
//        return $this->lsCommentaires;
//    }
//
//    public function addLsCommentaires(Commentaire $commentaire): self
//    {
//        if (!$this->lsCommentaires->contains($commentaire)) {
//            $this->lsCommentaires[] = $commentaire;
//            $commentaire->setEcritPar($this);
//        }
//
//        return $this;
//    }
//
//    public function removeLsCommentaires(Commentaire $commentaire): self
//    {
//        if ($this->lsCommentaires->removeElement($commentaire)) {
//            // set the owning side to null (unless already changed)
//            if ($commentaire->getEcritPar() === $this) {
//                $commentaire->setEcritPar(null);
//            }
//        }
//
//        return $this;
//    }
//
//    /**
//     * @return Collection|Message[]
//     */
//    public function getLsRessourcesUtilisateur(): Collection
//    {
//        return $this->lsEtatRessourceUtilisateur;
//    }
//
//    public function addLsRessourcesUtilisateur(EtatRessourceUtilisateur $etatRessourceUtilisateur): self
//    {
//        if (!$this->lsEtatRessourceUtilisateur->contains($etatRessourceUtilisateur)) {
//            $this->lsEtatRessourceUtilisateur[] = $etatRessourceUtilisateur;
//            $etatRessourceUtilisateur->setEcritPar($this);
//        }
//
//        return $this;
//    }
//
//    public function removeLsRessourcesUtilisateur(EtatRessourceUtilisateur $etatRessourceUtilisateur): self
//    {
//        if ($this->lsEtatRessourceUtilisateur->removeElement($etatRessourceUtilisateur)) {
//            // set the owning side to null (unless already changed)
//            if ($etatRessourceUtilisateur->getUtilisateur() === $this) {
//                $etatRessourceUtilisateur->setUtilisateur(null);
//            }
//        }
//
//        return $this;
//    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(?string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    public function getActivatedDate(): ?\DateTimeInterface
    {
        return $this->activatedDate;
    }

    public function initActivatedDate(): self
    {
        $this->activatedDate = new DateTime();

        return $this;
    }

    /**
     * @return MediaObject|null
     */
    public function getImage(): ?MediaObject
    {
        return $this->image;
    }

    /**
     * @param MediaObject|null $image
     */
    public function setImage(?MediaObject $image): void
    {
        $this->image = $image;
    }
}
