<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\TypeRessourceRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use App\Controller\TypeRessourceGetRessourcesAction;

/**
 * @ORM\Entity(repositoryClass=TypeRessourceRepository::class)
 * @ApiResource(
 *     itemOperations={
 *          "get",
 *          "put"={"security_post_denormalize"="is_granted('ROLE_SUPER_ADMIN')"},
 *          "patch"={"security_post_denormalize"="is_granted('ROLE_SUPER_ADMIN')"},
 *          "liste_ressources"={
 *          "method"="GET",
 *          "path"="/type_ressources/{id}/liste_ressources",
 *          "controller"=TypeRessourceGetRessourcesAction::class,
 *          }
 *     },
 *     collectionOperations={
 *          "get",
 *          "post"={"security_post_denormalize"="is_granted('ROLE_SUPER_ADMIN')"},
 *     },
 *    normalizationContext={
 *     "groups"={"type_ressource.read", "type_ressource.all"}
 *     },
 *     denormalizationContext={
 *      "groups"={"type_ressource.write", "type_ressource.all"}
 *     },
 *      attributes={
 *       "pagination_enabled"=false
 *      }
 * )
 */
class TypeRessource
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ApiProperty(identifier=false)
     */
    protected $id;

    /**
     * @var String
     * @ORM\Column(type="string", unique=true)
     * @ApiProperty(identifier=true)
     * @Gedmo\Slug(fields={"libelle"})
     * @Groups({
     *     "type_ressource.read",
     *     "ressource.read",
     *     "user.read",
     * })
     */
    protected $slug;

    /**
     * @var String
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Veuillez renseigner le libelle")
     * @Gedmo\Translatable()
     * @Groups({
     *     "type_ressource.all",
     *     "ressource.read",
     *     "user.read",
     * })
     */
    protected $libelle;

    /**
     * @Gedmo\Locale()
     */
    protected $locale;
//
//    /**
//     * @ORM\OneToMany(targetEntity=Ressource::class, mappedBy="typeRessource", orphanRemoval=true)
//     * @ApiProperty(readableLink=false, writableLink=false)
//     */
//    protected $lsRessources;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     * @Groups({
     *     "type_ressource.read"
     * })
     */
    protected $createdDate;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     * @Groups({
     *     "type_ressource.read"
     * })
     */
    protected $updatedDate;

//
//    /**
//     * @var DateTime
//     * @ORM\Column(type="datetime", nullable=true)
//     * @Groups({
//     *     "type_ressource.read"
//     * })
//     */
//    protected $deletedDate;
//
//    public function __construct()
//    {
//        $this->lsRessources = new ArrayCollection();
//    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreatedDate(): DateTime
    {
        return $this->createdDate;
    }

    public function getUpdatedDate(): ?DateTime
    {
        return $this->updatedDate;
    }
//
//    public function getDeletedDate(): ?DateTime
//    {
//        return $this->deletedDate;
//    }
//
//    public function delete(bool $deleted): self
//    {
//        if ($deleted) {
//            $this->deletedDate = new DateTime();
//        } else {
//            $this->deletedDate = new DateTime();
//        }
//    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getLibelle()
    {
        return $this->libelle;
    }

    public function getLocale()
    {
        return $this->locale;
    }

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }
//
//    /**
//     * @return Collection|Ressource[]
//     */
//    public function getLsRessources(): Collection
//    {
//        return $this->lsRessources;
//    }
//
//    public function addLsRessource(Ressource $lsRessource): self
//    {
//        if (!$this->lsRessources->contains($lsRessource)) {
//            $this->lsRessources[] = $lsRessource;
//            $lsRessource->setTypeRessource($this);
//        }
//
//        return $this;
//    }
//
//    public function removeLsRessource(Ressource $lsRessource): self
//    {
//        if ($this->lsRessources->removeElement($lsRessource)) {
//            // set the owning side to null (unless already changed)
//            if ($lsRessource->getTypeRessource() === $this) {
//                $lsRessource->setTypeRessource(null);
//            }
//        }
//
//        return $this;
//    }

}
