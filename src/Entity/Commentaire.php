<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CommentaireRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass=CommentaireRepository::class)
 * @ApiResource(attributes={
 *      "normalization_context"={"groups"={"commentaire.read","commentaire.all"}},
 *     "denormalization_context"={"groups"={"commentaire.write", "commentaire.all"}}
 * },itemOperations={
 *      "put"={"security_post_denormalize"="is_granted('ROLE_MODO') or (object.getEcritPar() == user)"},
 *      "patch"={"security_post_denormalize"="is_granted('ROLE_ADMIN') or (object.getCreateur() == user)"},
 *      "delete"={"security_post_denormalize"="is_granted('ROLE_ADMIN') or (object.getCreateur() == user)"}
 *     }
 * )
 */
class Commentaire
{

    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text")
     * @Groups({
     *     "commentaire.all"
     * })
     */
    protected $contenu;

//    , inversedBy="lsCommentaires"
    /**
     * @ORM\ManyToOne(targetEntity=Ressource::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({
     *     "commentaire.all"
     * })
     */
    protected $ecritPour;

//    , inversedBy="lsCommentaires"
    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({
     *     "commentaire.all"
     * })
     */
    protected $ecritPar;

    /**
     * @ORM\ManyToOne(targetEntity=Commentaire::class, inversedBy="lsCommentairesEnfants")
     * @ApiProperty(readableLink=false, writableLink=false)
     * @Groups({
     *     "commentaire.all"
     * })
     */
    protected $commentaireParent;

    /**
     * @ORM\OneToMany(targetEntity=Commentaire::class, mappedBy="commentaireParent")
     * @ApiProperty(readableLink=false, writableLink=false)
     * @Groups({
     *     "commentaire.read"
     * })
     */
    protected $lsCommentairesEnfants;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     * @Groups({
     *     "commentaire.read"
     * })
     */
    protected $createdDate;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     * @Groups({
     *     "commentaire.read"
     * })
     */
    protected $updatedDate;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({
     *     "commentaire.read"
     * })
     */
    protected $deletedDate;

    public function __construct()
    {
        $this->lsCommentairesEnfants = new ArrayCollection();
    }

    /**
     * @return int id
     */
    public function getId()
    {
        return $this->id;
    }

    public function getContenu(): ?string
    {
        return $this->contenu;
    }

    public function setContenu(string $contenu): self
    {
        $this->contenu = $contenu;

        return $this;
    }

    public function getEcritPour(): ?Ressource
    {
        return $this->ecritPour;
    }

    public function setEcritPour(?Ressource $ecritPour): self
    {
        $this->ecritPour = $ecritPour;

        return $this;
    }

    public function getEcritPar(): ?User
    {
        return $this->ecritPar;
    }

    public function setEcritPar(?User $ecritPar): self
    {
        $this->ecritPar = $ecritPar;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getLsCommentairesEnfants(): Collection
    {
        return $this->lsCommentairesEnfants;
    }

    public function addLsCommentairesEnfant(self $lsCommentairesEnfant): self
    {
        if (!$this->lsCommentairesEnfants->contains($lsCommentairesEnfant)) {
            $this->lsCommentairesEnfants[] = $lsCommentairesEnfant;
            $lsCommentairesEnfant->setCommentaireParent($this);
        }

        return $this;
    }

    public function removeLsCommentairesEnfant(self $lsCommentairesEnfant): self
    {
        if ($this->lsCommentairesEnfants->removeElement($lsCommentairesEnfant)) {
            // set the owning side to null (unless already changed)
            if ($lsCommentairesEnfant->getCommentaireParent() === $this) {
                $lsCommentairesEnfant->setCommentaireParent(null);
            }
        }

        return $this;
    }

    public function getCommentaireParent(): ?Commentaire
    {
        return $this->commentaireParent;
    }

    public function setCommentaireParent(?Commentaire $commentaireParent): self
    {
        $this->commentaireParent = $commentaireParent;

        return $this;
    }

    public function getCreatedDate(): DateTime
    {
        return $this->createdDate;
    }

    public function getUpdatedDate(): ?DateTime
    {
        return $this->updatedDate;
    }

    public function getDeletedDate(): ?DateTime
    {
        return $this->deletedDate;
    }

    public function delete(bool $delete): self
    {
        if($delete){
            $this->deletedDate = new DateTime();
        } else {
            $this->deletedDate = new DateTime();
        }
    }

}
