<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\EtatRessourceRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=EtatRessourceRepository::class)
 * @ApiResource(
 *     itemOperations={
 *          "get",
 *          "put"={"security_post_denormalize"="is_granted('ROLE_SUPER_ADMIN')"},
 *          "patch"={"security_post_denormalize"="is_granted('ROLE_SUPER_ADMIN')"},
 *     },
 *     collectionOperations={
 *          "get",
 *          "post"={"security_post_denormalize"="is_granted('ROLE_SUPER_ADMIN')"},
 *     },
 *     attributes={
 *      "normalization_context"={"groups"={"etat_ressource.read","etat_ressource.all"}},
 *     "denormalization_context"={"groups"={"etat_ressource.write", "etat_ressource.all"}}
 * })
 */
class EtatRessource
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ApiProperty(identifier=false)
     */
    protected $id;

    /**
     * @var String
     * @ORM\Column(type="string", unique=true)
     * @ApiProperty(identifier=true)
     * @Gedmo\Slug(fields={"libelle"})
     * @Groups({
     *     "etat_ressource.read"
     * })
     */
    protected $slug;

    /**
     * @var String
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Veuillez renseigner le libelle")
     * @Gedmo\Language
     * @Groups({
     *     "etat_ressource.all"
     * })
     */
    protected $libelle;

    /**
     * @Gedmo\Locale
     */
    protected $locale;
//
//    /**
//     * @ORM\OneToMany(targetEntity=EtatRessourceUtilisateur::class, mappedBy="etatRessource", orphanRemoval=true)
//     * @Groups({"etat_ressource.read"})
//     */
//    protected $lsRessourcesUtilisateurs;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     * @Groups({
     *     "etat_ressource.read"
     * })
     */
    protected $createdDate;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     * @Groups({
     *     "etat_ressource.read"
     * })
     */
    protected $updatedDate;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({
     *     "etat_ressource.read"
     * })
     */
    protected $deletedDate;
//
//    public function __construct()
//    {
//        $this->lsRessourcesUtilisateurs = new ArrayCollection();
//    }
//
//    /**
//     * @return Collection|EtatRessourceUtilisateur[]
//     */
//    public function getLsRessourcesUtilisateurs(): Collection
//    {
//        return $this->lsRessourcesUtilisateurs;
//    }
//
//    public function addLsRessourcesUtilisateur(EtatRessourceUtilisateur $lsRessourcesUtilisateur): self
//    {
//        if (!$this->lsRessourcesUtilisateurs->contains($lsRessourcesUtilisateur)) {
//            $this->lsRessourcesUtilisateurs[] = $lsRessourcesUtilisateur;
//            $lsRessourcesUtilisateur->setEtatRessource($this);
//        }
//
//        return $this;
//    }
//
//    public function removeLsRessourcesUtilisateur(EtatRessourceUtilisateur $lsRessourcesUtilisateur): self
//    {
//        if ($this->lsRessourcesUtilisateurs->removeElement($lsRessourcesUtilisateur)) {
//            // set the owning side to null (unless already changed)
//            if ($lsRessourcesUtilisateur->getEtatRessource() === $this) {
//                $lsRessourcesUtilisateur->setEtatRessource(null);
//            }
//        }
//
//        return $this;
//    }

    public function getCreatedDate(): DateTime
    {
        return $this->createdDate;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getUpdatedDate(): ?DateTime
    {
        return $this->updatedDate;
    }

    public function getDeletedDate(): ?DateTime
    {
        return $this->deletedDate;
    }

    public function delete(bool $delete): self
    {
        if ($delete) {
            $this->deletedDate = new DateTime();
        } else {
            $this->deletedDate = new DateTime();
        }
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getLibelle()
    {
        return $this->libelle;
    }

    public function getLocale()
    {
        return $this->locale;
    }

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }

}
