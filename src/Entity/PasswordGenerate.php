<?php


namespace App\Entity;


use ApiPlatform\Core\Annotation\ApiProperty;
use App\Service\PasswordGenerator;

/**
 * Class Password
 * @package App\Entity
 */
class PasswordGenerate
{

    /**
     * @var String
     * @ApiProperty(identifier=true)
     */
    protected $generatedPassword;

    public function __construct(PasswordGenerator $generator)
    {
        $this->generatedPassword = $generator->generate();
    }

    /**
     * @return String
     */
    public function getGeneratedPassword(): string
    {
        return $this->generatedPassword;
    }

}