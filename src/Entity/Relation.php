<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\RelationRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Controller\RelationActive;
/**
 * @ORM\Entity(repositoryClass=RelationRepository::class)
 * @ApiResource (
 *     itemOperations={
 *          "get"={"security_post_denormalize"="is_granted('ROLE_ADMIN') or (object.getDemandeur() == user) or (object.getReceveur() == user)"},
 *          "delete"={"security_post_denormalize"="is_granted('ROLE_ADMIN') or (object.getDemandeur() == user) or (object.getReceveur() == user)"},
 *          "active_relation"={
 *              "method"="POST",
 *              "path"="/relations/{id}/active",
 *              "controller"=RelationActive::class
 *          }
 *     },
 *     collectionOperations={
 *          "post"={"security_post_denormalize"="is_granted('ROLE_ADMIN') or (object.getDemandeur() == user) or (object.getReceveur() == user)"},
 *     },
 *     normalizationContext={"groups"={"relation.read","relation.all"}},
 *     denormalizationContext={"groups"={"relation.write", "relation.all"}}
 * )
 */
class Relation implements CreateByUser
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ApiProperty(identifier=true)
     * @Groups({
     *     "relation.all",
     *     "user.read"
     * })
     */
    protected $id;

//    , inversedBy="lsRelations"
    /**
     * @ORM\ManyToOne(targetEntity=TypeRelation::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({
     *     "relation.all",
     *     "user.read"
     * })
     */
    protected $typeRelation;

//    , inversedBy="lsRelations"
    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({
     *     "relation.all",
     *     "type_relation.read",
     *     "user.read"
     * })
     */
    protected $demandeur;

//    , inversedBy="relationsRecues"
    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({
     *     "relation.all",
     *     "type_relation.read",
     *     "user.read"
     * })
     */
    protected $receveur;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     * @Groups({
     *     "relation.read",
     *     "user.read"
     * })
     */
    protected $dateDemande;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({
     *     "relation.read",
     *     "user.read"
     * })
     */
    protected ?\DateTimeInterface $acceptedDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({
     *     "relation.read",
     *     "user.read"
     * })
     */
    protected ?\DateTimeInterface $refusedDate;

    public function getTypeRelation(): ?TypeRelation
    {
        return $this->typeRelation;
    }

    public function setTypeRelation(?TypeRelation $typeRelation): self
    {
        $this->typeRelation = $typeRelation;

        return $this;
    }

    public function getDemandeur(): ?User
    {
        return $this->demandeur;
    }

    public function setDemandeur(?User $demandeur): self
    {
        $this->demandeur = $demandeur;

        return $this;
    }

    public function getReceveur(): ?User
    {
        return $this->receveur;
    }

    public function setReceveur(?User $receveur): self
    {
        $this->receveur = $receveur;

        return $this;
    }

    public function getdateDemande(): DateTime
    {
        return $this->dateDemande;
    }

    public function getAcceptedDate(): ?\DateTimeInterface
    {
        return $this->acceptedDate;
    }

    public function getRefusedDate(): ?\DateTimeInterface
    {
        return $this->refusedDate;
    }

    public function accepte(bool $accepted): self
    {
        if ($accepted) {
            $this->acceptedDate = new DateTime();
            $this->refusedDate = null;
        } else {
            $this->acceptedDate = null;
            $this->refusedDate = new DateTime();
        }
        return $this;
    }

    public function getUser(): ?UserInterface
    {
        return $this->getDemandeur();
    }

    public function setUser(UserInterface $user)
    {
        $this->setDemandeur($user);
    }

    /**
     * @return int id
     */
    public function getId()
    {
        return $this->id;
    }
}
