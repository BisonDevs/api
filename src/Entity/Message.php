<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\MessageRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=MessageRepository::class)
 * @ApiResource(attributes={
 *      "normalization_context"={"groups"={"message.read","message.all"}},
 *     "denormalization_context"={"groups"={"message.write", "message.all"}}
 * })
 */
class Message
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="text")
     * @Groups({
     *     "message.all",
     * })
     */
    protected $contenu;

//    , inversedBy="lsMessages"
    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({
     *     "message.all",
     * })
     */
    protected $auteur;

//    , inversedBy="lsMessages"
    /**
     * @ORM\ManyToOne(targetEntity=Discussion::class)
     * @ORM\JoinColumn(nullable=false)
     * @ApiProperty(writableLink=true)
     * @Groups({
     *     "message.all",
     * })
     */
    protected $discussion;

    /**
     * @ORM\ManyToOne(targetEntity=Message::class, inversedBy="messageEnfants")
     * @ApiProperty(readableLink=false, writableLink=false)
     * @Groups({
     *     "message.all",
     * })
     */
    protected $messageParent;

    /**
     * @ORM\OneToMany(targetEntity=Message::class, mappedBy="messageParent")
     * @Groups({
     *     "message.read",
     * })
     */
    protected $messageEnfants;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     * @Groups({
     *     "message.read",
     * })
     */
    protected $createdDate;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     * @Groups({
     *     "message.read",
     * })
     */
    protected $updatedDate;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({
     *     "message.read",
     * })
     */
    protected $deletedDate;

    public function __construct()
    {
        $this->messageEnfants = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContenu(): ?string
    {
        return $this->contenu;
    }

    public function setContenu(string $contenu): self
    {
        $this->contenu = $contenu;

        return $this;
    }

    public function getAuteur(): ?User
    {
        return $this->auteur;
    }

    public function setAuteur(?User $auteur): self
    {
        $this->auteur = $auteur;

        return $this;
    }

    public function getDiscussion(): ?Discussion
    {
        return $this->discussion;
    }

    public function setDiscussion(?Discussion $discussion): self
    {
        $this->discussion = $discussion;

        return $this;
    }

    public function getMessageParent(): ?self
    {
        return $this->messageParent;
    }

    public function setMessageParent(?self $messageParent): self
    {
        $this->messageParent = $messageParent;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getMessageEnfants(): Collection
    {
        return $this->messageEnfants;
    }

    public function addMessageEnfant(self $messageEnfant): self
    {
        if (!$this->messageEnfants->contains($messageEnfant)) {
            $this->messageEnfants[] = $messageEnfant;
            $messageEnfant->setMessageParent($this);
        }

        return $this;
    }

    public function removeMessageEnfant(self $messageEnfant): self
    {
        if ($this->messageEnfants->removeElement($messageEnfant)) {
            // set the owning side to null (unless already changed)
            if ($messageEnfant->getMessageParent() === $this) {
                $messageEnfant->setMessageParent(null);
            }
        }

        return $this;
    }

    public function getCreatedDate(): DateTime
    {
        return $this->createdDate;
    }

    public function getUpdatedDate(): ?DateTime
    {
        return $this->updatedDate;
    }

    public function getDeletedDate(): ?DateTime
    {
        return $this->deletedDate;
    }

    public function setDeletedDate(DateTime $deletedDate): self
    {
        $this->deletedDate = $deletedDate;

        return $this;
    }

}
