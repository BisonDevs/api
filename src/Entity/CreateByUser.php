<?php


namespace App\Entity;

use Symfony\Component\Security\Core\User\UserInterface;

interface CreateByUser
{

    public function getUser(): ?UserInterface;

    public function setUser(UserInterface $user);

}