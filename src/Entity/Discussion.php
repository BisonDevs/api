<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\DiscussionRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=DiscussionRepository::class)
 * @ApiResource(attributes={
 *      "normalization_context"={"groups"={"discussion.read","discussion.all"}},
 *     "denormalization_context"={"groups"={"discussion.write", "discussion.all"}}
 * })
 */
class Discussion
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ApiProperty(identifier=false)
     */
    protected $id;

    /**
     * @var String
     * @ORM\Column(type="string", unique=true)
     * @ApiProperty(identifier=true)
     * @Gedmo\Slug(fields={"titre"})
     * @Groups({
     *     "discussion.read"
     * })
     */
    protected $slug;

    /**
     * @var String
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Veuillez renseigner le titre")
     * @Groups({
     *     "discussion.all"
     * })
     */
    protected $titre;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @Groups({
     *     "discussion.all"
     * })
     */
    protected $createur;

//    , inversedBy="lsDiscussions"
    /**
     * @ORM\ManyToMany(targetEntity=User::class)
     * @
     * @Groups({
     *     "discussion.all"
     * })
     */
    protected $participants;

//    , inversedBy="lsDiscussions"
    /**
     * @ORM\ManyToOne(targetEntity=Ressource::class)
     * @Groups({
     *     "discussion.all"
     * })
     */
    protected $ressourceSupport;

//
//    /**
//     * @ORM\OneToMany(targetEntity=Message::class, mappedBy="discussion", orphanRemoval=true)
//     * @Groups({
//     *     "discussion.read"
//     * })
//     */
//    protected $lsMessages;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     * @Groups({
     *     "discussion.read"
     * })
     */
    protected $createdDate;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     * @Groups({
     *     "discussion.read"
     * })
     */
    protected $updatedDate;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({
     *     "discussion.read"
     * })
     */
    protected $deletedDate;

    public function __construct()
    {
        $this->participants = new ArrayCollection();
        // $this->ressourceSupport = new ArrayCollection();
//        $this->lsMessages = new ArrayCollection();
    }

    public function getCreateur(): ?User
    {
        return $this->createur;
    }

    public function setCreateur(?User $createur): self
    {
        $this->createur = $createur;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getParticipants(): Collection
    {
        return $this->participants;
    }

    public function addParticipant(User $participant): self
    {
        if (!$this->participants->contains($participant)) {
            $this->participants[] = $participant;
        }

        return $this;
    }

    public function removeParticipant(User $participant): self
    {
        $this->participants->removeElement($participant);

        return $this;
    }

    /**
     * @return ?Ressource
     */
    public function getRessourceSupport(): ?Ressource
    {
        return $this->ressourceSupport;
    }

    public function setRessourceSupport(?Ressource $ressourceSupport): self
    {
            $this->ressourceSupport = $ressourceSupport;

        return $this;
    }

//
//    /**
//     * @return Collection|Message[]
//     */
//    public function getLsMessages(): Collection
//    {
//        return $this->lsMessages;
//    }
//
//    public function addLsMessage(Message $lsMessage): self
//    {
//        if (!$this->lsMessages->contains($lsMessage)) {
//            $this->lsMessages[] = $lsMessage;
//            $lsMessage->setDiscussion($this);
//        }
//
//        return $this;
//    }
//
//    public function removeLsMessage(Message $lsMessage): self
//    {
//        if ($this->lsMessages->removeElement($lsMessage)) {
//            // set the owning side to null (unless already changed)
//            if ($lsMessage->getDiscussion() === $this) {
//                $lsMessage->setDiscussion(null);
//            }
//        }
//
//        return $this;
//    }

    public function getCreatedDate(): DateTime
    {
        return $this->createdDate;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getUpdatedDate(): ?DateTime
    {
        return $this->updatedDate;
    }

    public function getDeletedDate(): ?DateTime
    {
        return $this->deletedDate;
    }

    public function delete(bool $delete): self
    {
        if($delete){
            $this->deletedDate = new DateTime();
        } else {
            $this->deletedDate = new DateTime();
        }
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getTitre()
    {
        return $this->titre;
    }

}
