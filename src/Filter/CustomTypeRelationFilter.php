<?php
// api/src/Filter/RegexpFilter.php

namespace App\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\Partage;
use App\Entity\Ressource;
use App\Entity\User;
use Doctrine\ORM\QueryBuilder;

final class CustomTypeRelationFilter extends AbstractContextAwareFilter
{
    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null)
    {

        // otherwise filter is applied to order and page as well
        if (
        !$this->isPropertyEnabled($property)
//            ||
//            !$this->isPropertyMapped($property, $resourceClass)
        ) {
            return;
        }

        $parameterName = $queryNameGenerator->generateParameterName($property); // Generate a unique parameter name to avoid collisions with other filters

        if (!is_array($value)) {
            $value = [$value];
        }

        switch ($property) {
            case "custom_type_relation_filter":
            case "custom_type_relation_filter[]":
                switch ($resourceClass) {
                    case Ressource::class:

                        $aliases = $queryBuilder->getAllAliases();

                        if (!in_array('p', $aliases)) {
                            $queryBuilder
                                ->leftJoin('App\Entity\Partage', 'p', 'WITH', 'o.id = p.ressourcePartage');
                        }

                        if (!in_array('tr', $aliases)) {
                            $queryBuilder
                                ->leftJoin('App\Entity\TypeRelation', 'tr', 'WITH', 'p.ciblePartage = tr.id');
                        }

                        $queryBuilder->andWhere(
                            $queryBuilder->expr()->in('tr.slug', $value)
                        );

                        break;
                }
                break;
        }
    }

    // This function is only used to hook in documentation generators (supported by Swagger and Hydra)
    public function getDescription(string $resourceClass): array
    {
        if (!$this->properties) {
            return [];
        }

        $description = [];
        foreach ($this->properties as $property => $strategy) {
            $description["$property"] = [
                'property' => $property,
                'type' => 'string',
                'required' => false,
                'swagger' => [
                    'description' => 'Custom type relation filter',
                    'name' => '',
                    'type' => '',
                ],
            ];
        }

        return $description;
    }

}