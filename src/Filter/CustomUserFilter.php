<?php
// api/src/Filter/RegexpFilter.php

namespace App\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\Ressource;
use App\Entity\User;
use Doctrine\ORM\QueryBuilder;

final class CustomUserFilter extends AbstractContextAwareFilter
{
    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null)
    {

        // otherwise filter is applied to order and page as well
        if (
        !$this->isPropertyEnabled($property)
//            ||
//            !$this->isPropertyMapped($property, $resourceClass)
        ) {
            return;
        }

        if (!empty($value)) {
            $parameterName = $queryNameGenerator->generateParameterName($property); // Generate a unique parameter name to avoid collisions with other filters
            switch ($property) {
                case "custom_user_filter":
                    switch ($resourceClass) {
                        case User::class:
                            $queryBuilder
                                ->andWhere(
                                    'CONCAT(o.nom, o.prenom) LIKE :' . $parameterName .
                                    ' or CONCAT(o.nom, \' \', o.prenom) LIKE :' . $parameterName .
                                    ' or o.email LIKE :' . $parameterName .
                                    ' or o.pseudo LIKE :' . $parameterName
                                )
                                ->setParameter($parameterName, '%' . $value . '%');
                            break;

                        case Ressource::class :

                            $aliases = $queryBuilder->getAllAliases();


                            if(!in_array('u', $aliases)){
                                $queryBuilder
                                    ->innerJoin('App\Entity\User', 'u', 'WITH', 'o.createur = u.id');
                            }

                            $queryBuilder
                                ->andWhere(
                                    'CONCAT(u.nom, u.prenom) LIKE :' . $parameterName .
                                    ' or CONCAT(u.nom, \' \', u.prenom) LIKE :' . $parameterName .
                                    ' or u.email LIKE :' . $parameterName .
                                    ' or u.pseudo LIKE :' . $parameterName
                                )
                                ->setParameter($parameterName, '%' . $value . '%');

                            break;
                    }
                    break;
            }
        }
    }

    // This function is only used to hook in documentation generators (supported by Swagger and Hydra)
    public function getDescription(string $resourceClass): array
    {
        if (!$this->properties) {
            return [];
        }

        $description = [];
        foreach ($this->properties as $property => $strategy) {
            $description["$property"] = [
                'property' => $property,
                'type' => 'string',
                'required' => false,
                'swagger' => [
                    'description' => 'Custom user filter',
                    'name' => '',
                    'type' => '',
                ],
            ];
        }

        return $description;
    }

}