<?php


namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use ApiPlatform\Core\Util\RequestAttributesExtractor;
use App\Entity\Partage;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Vich\UploaderBundle\Storage\StorageInterface;

final class ActivationPartageSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['activatePartage', EventPriorities::PRE_WRITE],
        ];
    }

    public function activatePartage(ViewEvent $event): void
    {
        $object = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (
            !$object instanceof Partage ||
            Request::METHOD_POST !== $method
        ) {
            return;
        }

        if ($object->getCiblePartage()->getSlug() != $_SERVER['SLUG_TYPE_RELATION_PUBLIC']) {
            $object->initActivatedDate();
        }

    }
}
