<?php

namespace App\Repository;

use App\Entity\Ressource;
use App\Entity\Relation;
use App\Entity\User;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\QueryException;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use stdClass;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use function Doctrine\ORM\QueryBuilder;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends PaginatedListRepository implements PasswordUpgraderInterface
{
    protected UserPasswordEncoderInterface $passwordEncoder;
    protected RequestStack $request;
    protected Security $security;

    public function __construct(ManagerRegistry $registry, UserPasswordEncoderInterface $passwordEncoder, TokenStorageInterface $tokenStorage, RequestStack $request, Security $security)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->request = $request;
        $this->security = $security;
        parent::__construct($registry, User::class, $tokenStorage);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     * @param UserInterface $user
     * @param string $newEncodedPassword
     * @throws Exception
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            $newEncodedPassword
        ));
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
    }

    /**
     * @param int $id
     * @param int $page
     * @param int $itemsPerPage
     * @return Paginator
     * @throws QueryException
     */
    public function getListeRessources(int $id, int $page = 1, int $itemsPerPage = 20): Paginator
    {
        $currentUser = $this->security->getToken()->getUser();

        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select('r')
            ->from(Ressource::class, 'r')
            ->where('r.createur = :user');

        // Si l'utilisateur envoyé est l'utilisateur connecté on il accède à tout ses données
        if (!$this->security->isGranted('SUPER_ADMIN') && (empty($currentUser) || !$currentUser instanceof User || $currentUser->getId() != $id)) {
            $queryBuilder->innerJoin('App\Entity\Partage', 'p', 'WITH', 'r.id = p.ressourcePartage');
            $queryBuilder->innerJoin('App\Entity\TypeRelation', 'tr', 'WITH', 'p.ciblePartage = tr.id');
            $queryBuilder->setParameter('slug', "%" . $_SERVER['SLUG_TYPE_RELATION_PUBLIC'] . "%");

            if (!empty($currentUser) && $currentUser instanceof User) {
                $sub = $this->getEntityManager()->createQueryBuilder();
                $sub->select('IDENTITY(ru.typeRelation)')
                    ->from(Relation::class, 'ru')
                    ->where($sub->expr()->isNotNull('ru.acceptedDate'))
                    ->andWhere($sub->expr()->isNull('ru.refusedDate'))
                    ->andWhere(
                        $sub->expr()->orX(
                            $queryBuilder->expr()->andX(
                                'ru.demandeur = :user',
                                'ru.receveur = :connected_user'
                            ),
                            $queryBuilder->expr()->andX(
                                'ru.demandeur = :connected_user',
                                'ru.receveur = :user'
                            )
                        )
                    );

                $queryBuilder->andWhere(
                    $queryBuilder->expr()->orX(
                        'tr.slug LIKE :slug',
                        $queryBuilder->expr()->in('tr.id', $sub->getDQL())
                    )
                );

                $queryBuilder->setParameter('connected_user', $currentUser->getId());

            } else {
                $queryBuilder->andWhere('tr.slug LIKE :slug');
            }

        }

        $queryBuilder->setParameter('user', $id);

        return $this->getPaginatedList($queryBuilder, $page, $itemsPerPage);
    }

    /**
     * @param int $id
     * @param int $page
     * @param int $itemsPerPage
     * @return Paginator
     * @throws QueryException
     */
    public function getListeRelations(int $id, int $page = 1, int $itemsPerPage = 20): Paginator
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select('r')
            ->from(Relation::class, 'r')
            ->where('r.acceptedDate IS NOT NULL')
            ->andWhere('r.refusedDate IS NULL')
            ->andWhere(
                $queryBuilder->expr()->orX('r.demandeur = :user', 'r.receveur = :user')
            )
            ->setParameter('user', $id);

        return $this->getPaginatedList($queryBuilder, $page, $itemsPerPage, false);
    }

    /**
     * @param int $id
     * @param int $page
     * @param int $itemsPerPage
     * @return Paginator
     * @throws QueryException
     */
    public function getListeDemandesEnAttente(int $id, int $page = 1, int $itemsPerPage = 20): Paginator
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select('r')
            ->from(Relation::class, 'r')
            ->where('r.acceptedDate IS NULL')
            ->andWhere('r.refusedDate IS NULL')
            ->andWhere(
                $queryBuilder->expr()->orX('r.demandeur = :user')
            )
            ->setParameter('user', $id);

        return $this->getPaginatedList($queryBuilder, $page, $itemsPerPage, false);
    }

    /**
     * @param int $id
     * @param int $page
     * @param int $itemsPerPage
     * @return Paginator
     * @throws QueryException
     */
    public function getListeRelationsEnAttente(int $id, int $page = 1, int $itemsPerPage = 20): Paginator
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select('r')
            ->from(Relation::class, 'r')
            ->where('r.acceptedDate IS NULL')
            ->andWhere('r.acceptedDate IS NULL')
            ->andWhere('r.refusedDate IS NULL')
            ->andWhere(
                $queryBuilder->expr()->orX('r.receveur = :user')
            )
            ->setParameter('user', $id);

        return $this->getPaginatedList($queryBuilder, $page, $itemsPerPage, false);
    }

    /**
     * @param int $id
     * @param int $page
     * @param int $itemsPerPage
     * @return Paginator
     * @throws QueryException
     */
    public function getListeUtilisateurDispoRelation(int $id, int $page = 1, int $itemsPerPage = 20): Paginator
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder->select('u')
            ->from(User::class, 'u')
            ->andWhere('u.id <> :user')
            ->setParameter('user', $id);

        return $this->getPaginatedList($queryBuilder, $page, $itemsPerPage);
    }

    public function getInfo(User $user)
    {

        $connectedUser = $this->security->getToken()->getUser();

        if (!empty($connectedUser) && $connectedUser instanceof User && !empty($connectedUser->getId()) && $connectedUser->getId() != $user->getId()) {
            $user->relations = $this->getEntityManager()->getRepository(Relation::class)->getRelationsBetweenUsers($connectedUser->getId(), $user->getId());
        }

        return $user;

    }
}
