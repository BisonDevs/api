<?php

namespace App\Repository;

use App\Entity\EtatRessource;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EtatRessource|null find($id, $lockMode = null, $lockVersion = null)
 * @method EtatRessource|null findOneBy(array $criteria, array $orderBy = null)
 * @method EtatRessource[]    findAll()
 * @method EtatRessource[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EtatRessourceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EtatRessource::class);
    }

    // /**
    //  * @return EtatRessource[] Returns an array of EtatRessource objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EtatRessource
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
