<?php

namespace App\Repository;

use App\Entity\CategorieRessource;
use App\Entity\Ressource;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Query\QueryException;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @method CategorieRessource|null find($id, $lockMode = null, $lockVersion = null)
 * @method CategorieRessource|null findOneBy(array $criteria, array $orderBy = null)
 * @method CategorieRessource[]    findAll()
 * @method CategorieRessource[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategorieRessourceRepository extends PaginatedListRepository
{
    public function __construct(
        ManagerRegistry $registry,
        TokenStorageInterface $tokenStorage
    ) {
        parent::__construct($registry, CategorieRessource::class, $tokenStorage);
    }

    /**
     * @param int $id
     * @param int $page
     * @param int $itemsPerPage
     * @return Paginator
     * @throws QueryException
     */
    public function getListeRessources(int $id, int $page = 1, int $itemsPerPage = 20): Paginator
    {

        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select('r')
            ->from(Ressource::class, 'r')
            ->where('r.categorieRessource = :idCateg')
            ->setParameter('idCateg', $id);

        return $this->getPaginatedList($queryBuilder, $page, $itemsPerPage);

    }

}
