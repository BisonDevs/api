<?php

namespace App\Repository;

use App\Entity\Relation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Relation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Relation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Relation[]    findAll()
 * @method Relation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RelationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Relation::class);
    }

    // /**
    //  * @return Relation[] Returns an array of Relation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Relation
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getRelationsBetweenUsers($user1, $user2)
    {
        $query = $this->createQueryBuilder('r');
        $query->where(
            $query->expr()->orX(
                $query->expr()->andX(
                    'r.demandeur = :user1',
                    'r.receveur = :user2'
                ),
                $query->expr()->andX(
                    'r.demandeur = :user2',
                    'r.receveur = :user1'
                )
            )
        )
            ->setParameter('user1', $user1)
            ->setParameter('user2', $user2);

        return $query->getQuery()->getResult();

    }

}
