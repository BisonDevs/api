<?php

namespace App\Repository;

use App\Entity\EtatRessourceUtilisateur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EtatRessourceUtilisateur|null find($id, $lockMode = null, $lockVersion = null)
 * @method EtatRessourceUtilisateur|null findOneBy(array $criteria, array $orderBy = null)
 * @method EtatRessourceUtilisateur[]    findAll()
 * @method EtatRessourceUtilisateur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EtatRessourceUtilisateurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EtatRessourceUtilisateur::class);
    }

    // /**
    //  * @return EtatRessourceUtilisateur[] Returns an array of EtatRessourceUtilisateur objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EtatRessourceUtilisateur
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
