<?php

namespace App\Repository;

use App\Entity\CategorieRessource;
use App\Entity\Ressource;
use App\Entity\TypeRessource;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Query\QueryException;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @method TypeRessource|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeRessource|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeRessource[]    findAll()
 * @method TypeRessource[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeRessourceRepository extends PaginatedListRepository
{
    public function __construct(
        ManagerRegistry $registry,
        TokenStorageInterface $tokenStorage
    )
    {
        parent::__construct($registry, TypeRessource::class, $tokenStorage);
    }

    /**
     * @param int $id
     * @param int $page
     * @param int $itemsPerPage
     * @return Paginator
     * @throws QueryException
     */
    public function getListeRessources(int $id, int $page = 1, int $itemsPerPage = 20): Paginator
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select('r')
            ->from(Ressource::class, 'r')
            ->where('r.typeRessource = :idType')
            ->setParameter('idType', $id);

        return $this->getPaginatedList($queryBuilder, $page, $itemsPerPage);
    }

}
