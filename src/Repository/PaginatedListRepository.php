<?php


namespace App\Repository;


use App\Entity\CategorieRessource;
use App\Entity\Ressource;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Query\QueryException;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

abstract class PaginatedListRepository extends  ServiceEntityRepository
{
    private TokenStorageInterface $tokenStorage;

    public function __construct(
        ManagerRegistry $registry,
        $class,
        TokenStorageInterface $tokenStorage
    ) {
        $this->tokenStorage = $tokenStorage;
        parent::__construct($registry, $class);
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param int $page
     * @param int $itemsPerPage
     * @param bool $fetchJoinCollection
     * @return Paginator
     * @throws QueryException
     */
    public function getPaginatedList(QueryBuilder $queryBuilder, int $page, int $itemsPerPage, bool $fetchJoinCollection = true): Paginator
    {
        $firstResult = ($page -1) * $itemsPerPage;

        $criteria = Criteria::create()
            ->setFirstResult($firstResult)
            ->setMaxResults($itemsPerPage);
        $queryBuilder->addCriteria($criteria);

        return new Paginator($queryBuilder, $fetchJoinCollection);
    }
}