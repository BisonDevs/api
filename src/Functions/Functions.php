<?php

namespace App\Functions;

class Functions {

    public static function urlServer()
    {
        if (isset($_SERVER['HTTPS'])) {
            $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
        } else {
            $protocol = 'http';
        }
        return $protocol . "://" . $_SERVER['HTTP_HOST'];
    }

    public static function urlCall()
    {
        return self::urlServer() . $_SERVER['REQUEST_URI'];
    }

    public static function getRolesList()
    {
        return self::getRolesBOList() + self::getRolesFOList();
    }

    public static function getRolesBOList()
    {
        return [
            20 => 'ROLE_SUPER_ADMIN',
            15 => 'ROLE_ADMIN',
            10 => 'ROLE_MODO'
        ];
    }

    public static function getRolesFOList()
    {
        return [
            5 => 'ROLE_CITOYEN',
            0 => 'ROLE_USER'
        ];
    }

}
