<?php

namespace App\Service;

use App\Entity\User;
use App\Functions\Functions;
use DateInterval;
use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query\Expr\Func;
use Exception;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use stdClass;
use Symfony\Component\HttpFoundation\JsonResponse;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class UserActiveAccount
{

    private $twig;
    private $mailer;
    private $jwt;
    private $entityManager;
    private $separator = "__";
    private $minutes_to_add = 15;

    public function __construct(Environment $twig, SendMail $mailer, JWTTokenManagerInterface $jwt, EntityManagerInterface $entityManager)
    {
        $this->twig = $twig;
        $this->mailer = $mailer;
        $this->jwt = $jwt;
        $this->entityManager = $entityManager;
    }

    /**
     * @param User $data
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws Exception
     */
    public function sendMailActivation($data)
    {
//        $token = $this->jwt->create($data);
        $token = $this->encodeToken($data);

        $url = $_SERVER['WEB_SITE_URL'] . '/utilisateur/active/' . $token;
        $utilisateur = $data->getNom() . " " . $data->getPrenom();

        $text = $utilisateur . ",\n\n";
        $text .= "Nous vous souhaitons la bienvenue sur Ressource Relationnelle.\n";
        $text .= "Si vous n'êtes pas à l'origine de la demande d'inscription, veuillez ne pas prendre en compte cet email.\n";
        $text .= "Pour activer votre compte veuillez cliquer sur le lien suivant :\n" . $url . "\n\n";
        $text .= "L'équipe Ressource Relationnelle.";

        $html = $this->twig->render(
            'email/activation.html.twig',
            [
                "url_api" => Functions::urlServer(),
                "utilisateur" => $utilisateur,
                "url_web_active_compte" => $url
            ]
        );

        $this->mailer->sendMail(
            'Activation de votre compte',
            [
                [
                    "Name" => $utilisateur,
                    "Email" => $data->getEmail()
                ]
            ],
            $text,
            $html
        );

    }

    /**
     * @param User $user
     * @return string
     * @throws Exception
     */
    protected function encodeToken(User $user)
    {
        $endTime = new DateTime();
        $actualTime = $endTime;
        $endTime->add(new DateInterval('PT' . $this->minutes_to_add . 'M'));

        return base64_encode(
            $user->getUuid() . $this->separator .
            $actualTime->getTimestamp() . $this->separator .
            $endTime->getTimestamp()
        );
    }

    /**
     * @param String $token
     * @return false|string[]
     */
    protected function decodeToken(string $token)
    {
        return explode($this->separator, base64_decode($token));
    }

    /**
     * @param string $token
     * @return JsonResponse
     */
    public function activeAccount(string $token): JsonResponse
    {

        $tokenInfos = $this->decodeToken($token);


        $response = new stdClass();
        $response->user = $tokenInfos[0] ?? null;
        $user = null;
        $time = new DateTime();

        if (!empty($response->user)) {
            $user = $this->entityManager->getRepository(User::class)->findOneBy(
                [
                    'uuid' => $response->user
                ]
            );
        }

        if (!empty($user)) {
            if (!empty($user->getActivatedDate())) {
                $message = "Le compte est déjà actif";
                $code = 200;
            } else if (!empty($tokenInfos[2]) && $tokenInfos[2] >= $time->getTimestamp()) {

                $user->initActivatedDate();

                $this->entityManager->persist($user);
                $this->entityManager->flush();

                $message = "Le compte a bien été validé";
                $code = 200;

            } else {
                $message = "La date limite de validation est dépassée";
                $code = 401;
            }

        } else {
            $message = "Le compte utilisateur n'est pas valide";
            $code = 404;
        }

        $response->message = $message;
        $response->code = $code;

        return new JsonResponse($response, $code);

    }
}