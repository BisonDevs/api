<?php


namespace App\Service;


use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\MongoDBException;
use Doctrine\ODM\MongoDB\Tools\Console\Helper\DocumentManagerHelper;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Symfony\Component\Uid\Uuid;

class UuidService
{
    /**
     * @var ObjectManager
     */
    private $om;

    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * Génère un uuid unique
     *
     * @param $class
     * @return string
     */
    public function generateUuid($class): string
    {
        do {

            $uuid = Uuid::v4();

            $uuid_exist = $this->om
                ->getRepository($class)
                ->findBy(['uuid' => $uuid]);

        } while (!empty($uuid_exist));

        return $uuid;
    }
}