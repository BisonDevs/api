<?php


namespace App\Service;


class PasswordGenerator
{

    public function generate()
    {
        $all = '';
        $pass = array();
        $nbCaracInPassword = [
            'lower' => [
                'abcdefghijklmnopqrstuvwxyz',
                2
            ],
            'upper' => [
                'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
                2
            ],
            'number' => [
                '1234567890',
                2
            ],
            'spe' => [
                '~#|^&-_=$*^,;:!?.%',
                2
            ]
        ];

        foreach ($nbCaracInPassword as $typeCarac => $infos) {
            for ($i = 0; $i < $infos[1]; $i++) {
                $n = rand(0, strlen($infos[0]) - 1);
                $pass[] = $infos[0][$n];
            }
            $all .= $infos[0];
        }

        $i = 0;

        $all = str_split($all);

        while (count($pass) < 20) {
            $n = rand(0, count($all) - 1);
            $slice = rand(0, count($pass));
            $pass = array_slice($pass, 0, $slice, true) +
                array(count($pass) => $all[$n]) +
                array_slice($pass, $slice, count($pass), true);
            $i ++;
            if($i >= 20){
                exit;
            }
        }

        return implode($pass);
    }

}