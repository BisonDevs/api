<?php


namespace App\Service;


use Exception;
use Mailjet\Client;
use Mailjet\Resources;

class SendMail
{

    /**
     * @param $subject
     * @param $to
     * @param $text
     * @param $html
     * @throws Exception
     */
    public function sendMail($subject, $to, $text, $html)
    {
        $mj = new Client($_SERVER['MJ_APIKEY_PUBLIC'], $_SERVER['MJ_APIKEY_PRIVATE'], true, ['version' => 'v3.1']);
        $body = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => "bisondevs@gmail.com",
                        'Name' => "Ressource Relationnelle"
                    ],
                    'To' => $to,
                    'Subject' => $subject,
                    'TextPart' => $text,
                    'HTMLPart' => $html
                ]
            ]
        ];

        $response = $mj->post(Resources::$Email, ['body' => $body]);
        if(!$response->success()){
            throw new Exception("Le mail n'a pu s'envoyer => ".print_r($response->getBody(), true));
        }
    }

}