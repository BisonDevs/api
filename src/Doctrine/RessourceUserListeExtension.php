<?php


namespace App\Doctrine;


use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryItemExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\Partage;
use App\Entity\Relation;
use App\Entity\User;
use App\Functions\Functions;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Role\RoleHierarchy;
use Symfony\Component\Security\Core\Security;

final class RessourceUserListeExtension implements QueryCollectionExtensionInterface, QueryItemExtensionInterface
{
    private Security $security;
    private RequestStack $request;

    public function __construct(Security $security, RequestStack $request)
    {
        $this->security = $security;
        $this->request = $request;
    }

    public function applyToCollection(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null): void
    {
        if ($resourceClass == "App\Entity\Ressource") {

            /**
             * @var User | null
             */
            $user = $this->security->getUser();

            $roles = Functions::getRolesList();

            $powerfullRole = 0;

            if (!empty($user)) {

                foreach ($user->getRoles() as $role) {
                    $powerfullRole = max($powerfullRole, array_keys($roles, $role)[0] ?? 0);
                }
            }

            $libPowerfullRole = $roles[$powerfullRole];

            // Si ce n'est pas un admin
            if (!in_array($libPowerfullRole, ['ROLE_SUPER_ADMIN', 'ROLE_ADMIN']) || !empty($this->request->getCurrentRequest()->get("getRessourceAsUser"))) {

                // On affiche les ressources public
                $queryBuilder->distinct(true);
                $queryBuilder->leftJoin('App\Entity\Partage', 'p', 'WITH', 'o.id = p.ressourcePartage');
                $queryBuilder->leftJoin('App\Entity\TypeRelation', 'tr', 'WITH', 'p.ciblePartage = tr.id');
                $queryBuilder->where('tr.slug LIKE :slug');

                // Si ce n'est pas un modo, ces ressources doivent être actives
                if ($libPowerfullRole != 'ROLE_MODO' || !empty($this->request->getCurrentRequest()->get("getRessourceAsUser"))) {
                    $queryBuilder->andwhere('p.activatedDate IS NOT NULL');
                } else {
                    $queryBuilder->andwhere('p.activatedDate IS NULL');
                }

                // Si c'est un citoyen ou un modo on peut accéder aussi à ses ressources et à c'elles de nos relations qui sont partagées avec nous
                if (
                    in_array($libPowerfullRole, ['ROLE_CITOYEN']) ||
                    (!in_array($libPowerfullRole, ['ROLE_SUPER_ADMIN', 'ROLE_ADMIN', 'ROLE_MODO']) && !empty($this->request->getCurrentRequest()->get("getRessourceAsUser")))
                ) {

                    $queryBuilder->leftJoin(
                        'App\Entity\Relation',
                        'rel',
                        'WITH',
                        'tr.id = rel.typeRelation'
                    );
                    $queryBuilder->orWhere(
                        $queryBuilder->expr()->andX(
                            'rel.acceptedDate IS NOT NULL',
                            'rel.refusedDate IS NULL',
                            $queryBuilder->expr()->orX(
                                'rel.demandeur = :current_user',
                                'rel.receveur = :current_user'
                            )
                        ),
                        'o.createur = :current_user'
                    );
                    $queryBuilder->setParameter('current_user', $user->getId());
                }

                $queryBuilder->setParameter('slug', "%" . $_SERVER['SLUG_TYPE_RELATION_PUBLIC'] . "%");
                $queryBuilder->select('o');
            }

            $queryBuilder->orderBy('o.updatedDate', 'DESC');
        }
    }

    public function applyToItem(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, array $identifiers, string $operationName = null, array $context = []): void
    {
    }

    private function addWhere(QueryBuilder $queryBuilder, string $resourceClass): void
    {
    }
}
