<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201230191208 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3EC04A43989D9B62 ON categorie_ressource (slug)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C0B9F90F989D9B62 ON discussion (slug)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_630D03A8989D9B62 ON etat_ressource (slug)');
        $this->addSql('ALTER TABLE relation ADD refused_date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE ressource CHANGE contenu contenu JSON NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_939F4544989D9B62 ON ressource (slug)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_AF7660B5989D9B62 ON type_relation (slug)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_27590454989D9B62 ON type_ressource (slug)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D64986CC499D ON user (pseudo)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_3EC04A43989D9B62 ON categorie_ressource');
        $this->addSql('DROP INDEX UNIQ_C0B9F90F989D9B62 ON discussion');
        $this->addSql('DROP INDEX UNIQ_630D03A8989D9B62 ON etat_ressource');
        $this->addSql('ALTER TABLE relation DROP refused_date');
        $this->addSql('DROP INDEX UNIQ_939F4544989D9B62 ON ressource');
        $this->addSql('ALTER TABLE ressource CHANGE contenu contenu LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('DROP INDEX UNIQ_AF7660B5989D9B62 ON type_relation');
        $this->addSql('DROP INDEX UNIQ_27590454989D9B62 ON type_ressource');
        $this->addSql('DROP INDEX UNIQ_8D93D64986CC499D ON user');
    }
}
