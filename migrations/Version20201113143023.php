<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201113143023 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE categorie_ressource DROP activated_date');
        $this->addSql('ALTER TABLE discussion DROP activated_date');
        $this->addSql('ALTER TABLE etat_ressource DROP activated_date');
        $this->addSql('ALTER TABLE message DROP activated_date');
        $this->addSql('ALTER TABLE relation ADD accepted_date DATETIME DEFAULT NULL, CHANGE created_date date_demande DATETIME NOT NULL');
        $this->addSql('ALTER TABLE type_relation DROP activated_date');
        $this->addSql('ALTER TABLE type_ressource DROP activated_date');
        $this->addSql('ALTER TABLE user ADD pseudo VARCHAR(30) DEFAULT NULL, DROP activated_date');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE categorie_ressource ADD activated_date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE discussion ADD activated_date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE etat_ressource ADD activated_date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE message ADD activated_date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE relation DROP accepted_date, CHANGE date_demande created_date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE type_relation ADD activated_date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE type_ressource ADD activated_date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD activated_date DATETIME DEFAULT NULL, DROP pseudo');
    }
}
