<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210113154736 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE commentaire DROP activated_date');
        $this->addSql('ALTER TABLE etat_ressource_utilisateur ADD id INT AUTO_INCREMENT NOT NULL, DROP PRIMARY KEY, ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE partage ADD id INT AUTO_INCREMENT NOT NULL, DROP PRIMARY KEY, ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE relation ADD id INT AUTO_INCREMENT NOT NULL, DROP PRIMARY KEY, ADD PRIMARY KEY (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE commentaire ADD activated_date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE etat_ressource_utilisateur MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE etat_ressource_utilisateur DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE etat_ressource_utilisateur DROP id');
        $this->addSql('ALTER TABLE etat_ressource_utilisateur ADD PRIMARY KEY (ressource_id, utilisateur_id, etat_ressource_id)');
        $this->addSql('ALTER TABLE partage MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE partage DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE partage DROP id');
        $this->addSql('ALTER TABLE partage ADD PRIMARY KEY (partageur_id, cible_partage_id, ressource_partage_id)');
        $this->addSql('ALTER TABLE relation MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE relation DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE relation DROP id');
        $this->addSql('ALTER TABLE relation ADD PRIMARY KEY (type_relation_id, demandeur_id, receveur_id)');
    }
}
