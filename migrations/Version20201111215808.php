<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201111215808 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE discussion (id INT AUTO_INCREMENT NOT NULL, createur_id INT DEFAULT NULL, INDEX IDX_C0B9F90F73A201E5 (createur_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE discussion_user (discussion_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_A8FD7A7F1ADED311 (discussion_id), INDEX IDX_A8FD7A7FA76ED395 (user_id), PRIMARY KEY(discussion_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE discussion_ressource (discussion_id INT NOT NULL, ressource_id INT NOT NULL, INDEX IDX_10F998121ADED311 (discussion_id), INDEX IDX_10F99812FC6CD52A (ressource_id), PRIMARY KEY(discussion_id, ressource_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE message (id INT AUTO_INCREMENT NOT NULL, auteur_id INT NOT NULL, discussion_id INT NOT NULL, message_parent_id INT DEFAULT NULL, contenu LONGTEXT NOT NULL, INDEX IDX_B6BD307F60BB6FE6 (auteur_id), INDEX IDX_B6BD307F1ADED311 (discussion_id), INDEX IDX_B6BD307F18C3CC46 (message_parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE partage (partageur_id INT NOT NULL, cible_partage_id INT NOT NULL, ressource_partage_id INT NOT NULL, created_date DATETIME NOT NULL, INDEX IDX_8B929E6EF25B9E32 (partageur_id), INDEX IDX_8B929E6E5C72246C (cible_partage_id), INDEX IDX_8B929E6E1CA11E61 (ressource_partage_id), PRIMARY KEY(partageur_id, cible_partage_id, ressource_partage_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE relation (type_relation_id INT NOT NULL, demandeur_id INT NOT NULL, receveur_id INT NOT NULL, created_date DATETIME NOT NULL, INDEX IDX_62894749794F46CA (type_relation_id), INDEX IDX_6289474995A6EE59 (demandeur_id), INDEX IDX_62894749B967E626 (receveur_id), PRIMARY KEY(type_relation_id, demandeur_id, receveur_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_relation (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE discussion ADD CONSTRAINT FK_C0B9F90F73A201E5 FOREIGN KEY (createur_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE discussion_user ADD CONSTRAINT FK_A8FD7A7F1ADED311 FOREIGN KEY (discussion_id) REFERENCES discussion (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE discussion_user ADD CONSTRAINT FK_A8FD7A7FA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE discussion_ressource ADD CONSTRAINT FK_10F998121ADED311 FOREIGN KEY (discussion_id) REFERENCES discussion (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE discussion_ressource ADD CONSTRAINT FK_10F99812FC6CD52A FOREIGN KEY (ressource_id) REFERENCES ressource (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F60BB6FE6 FOREIGN KEY (auteur_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F1ADED311 FOREIGN KEY (discussion_id) REFERENCES discussion (id)');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F18C3CC46 FOREIGN KEY (message_parent_id) REFERENCES message (id)');
        $this->addSql('ALTER TABLE partage ADD CONSTRAINT FK_8B929E6EF25B9E32 FOREIGN KEY (partageur_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE partage ADD CONSTRAINT FK_8B929E6E5C72246C FOREIGN KEY (cible_partage_id) REFERENCES type_relation (id)');
        $this->addSql('ALTER TABLE partage ADD CONSTRAINT FK_8B929E6E1CA11E61 FOREIGN KEY (ressource_partage_id) REFERENCES ressource (id)');
        $this->addSql('ALTER TABLE relation ADD CONSTRAINT FK_62894749794F46CA FOREIGN KEY (type_relation_id) REFERENCES type_relation (id)');
        $this->addSql('ALTER TABLE relation ADD CONSTRAINT FK_6289474995A6EE59 FOREIGN KEY (demandeur_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE relation ADD CONSTRAINT FK_62894749B967E626 FOREIGN KEY (receveur_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE discussion_user DROP FOREIGN KEY FK_A8FD7A7F1ADED311');
        $this->addSql('ALTER TABLE discussion_ressource DROP FOREIGN KEY FK_10F998121ADED311');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307F1ADED311');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307F18C3CC46');
        $this->addSql('ALTER TABLE partage DROP FOREIGN KEY FK_8B929E6E5C72246C');
        $this->addSql('ALTER TABLE relation DROP FOREIGN KEY FK_62894749794F46CA');
        $this->addSql('DROP TABLE discussion');
        $this->addSql('DROP TABLE discussion_user');
        $this->addSql('DROP TABLE discussion_ressource');
        $this->addSql('DROP TABLE message');
        $this->addSql('DROP TABLE partage');
        $this->addSql('DROP TABLE relation');
        $this->addSql('DROP TABLE type_relation');
    }
}
